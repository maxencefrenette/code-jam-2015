import zlib
import binascii

classesToBuild = ["InputParser", "Perceptron", "hr"]
buildName = "codejam-hr"

file = open("perceptron.txt", encoding="utf-8")
buildBuffer = "weights = " + file.read() + "\n"
file.close()
file = open("w1.txt", encoding="utf-8")
buildBuffer += "weights1 = " + file.read() + "\n"
file.close()
file = open("w2.txt", encoding="utf-8")
buildBuffer += "weights2 = " + file.read() + "\n"
file.close()

for aClass in classesToBuild:
	file = open(aClass + ".py", encoding="utf-8")
	content = file.read()
	for className in classesToBuild:
		content = content.replace("from " + className + " import " + className, "")
		content = content.replace("import " + className, "")
	buildBuffer += content + "\n"
	file.close()

buildFile = open(buildName + ".py", "w")
buildFile.write(buildBuffer)
buildFile.close()

print("Build completed")
