def parseLine(data, hasOutputs):
	#Removes trailling newlines
	data = data.replace('\n', '')
	#Splits data into columns
	patient = data.split('\t')
	
	patientId = patient[0]
	
	#If the passed data include output fields
	if (hasOutputs):
		patientInputs = patient[1:-3]
		patientOutputs = patient[-3:]
	else:
		patientInputs = patient[1:]
		patientOutputs = []
	
	#Convert non-numerical values to numerical values
	processTernaryValue('M', 'N', 'F', patientInputs, 0)
	processTernaryValue('YES', 'N', 'NO', patientInputs, 3)
	processTernaryValue('YES', 'N', 'NO', patientInputs, 4)
	processTernaryValue('YES', 'N', 'NO', patientInputs, 5)
	processTernaryValue('Yes', 'N', 'No', patientInputs, 6)
	processTernaryValue('POS', 'ND', 'NEG', patientInputs, 7)
	processTernaryValue('POS', 'ND', 'NEG', patientInputs, 8)
	processTernaryValue('POS', 'NotDone', 'NEG', patientInputs, 9)
	convertMultipleValues(['StdAraC-Plus', 'HDAC-Plus', 'Anthra-Plus', 'Flu-HDAC', 'Anthra-HDAC'], patientInputs, 10)
	
	#Convert string to double values and 'NA' to 0 for other columns
	for i in [1, 2] + list(range(11, len(patientInputs))):
		try:
			patientInputs[i] = float(patientInputs[i])
		except Exception as e:
			patientInputs[i] = 0
	
	for i in range(1, len(patientOutputs)):
		try:
			patientOutputs[i] = float(patientOutputs[i])
		except Exception as e:
			patientOutputs[i] = 0
	
	patientInputs = flatten2d(patientInputs)
	
	return patientId, patientInputs, patientOutputs

def parseFromFile(filePath):
	file = open(filePath, encoding='utf-8')
	#Will contain the parsed data of all the patients
	patientsInputsArray = []
	patientsOutputsArray = []
	
	#Parse the data of every patient
	for line in file:
		#Checks if it is a valid line
		if (line[0:5] != 'train'):
			continue
		#Parses the line
		id, inputs, outputs = parseLine(line, True)
		#Stores it in the patients array
		patientsInputsArray.append(inputs)
		patientsOutputsArray.append(outputs)
	
	return patientsInputsArray, patientsOutputsArray

def processTernaryValue(upValue, middleValue, downValue, array, index):
	replacement = [None, None, None]
	if array[index] == upValue:
		replacement[0] = 1
	elif array[index] == middleValue:
		replacement[0] = 0
	elif array[index] == downValue:
		replacement[0] = -1
	else:
		raise Exception('Value is not one of the three possibilities')
	
	replacement[1] = 1 if array[index] == upValue else 0
	replacement[2] = 1 if array[index] == downValue else 0
	
	array[index] = replacement

def convertMultipleValues(values, array, index):
	replacement = [];
	for value in values:
		if (array[index] == value):
			replacement += [1, 1]
		else:
			replacement += [-1, 0]
	array[index] = replacement

def flatten2d(array):
	flatArray = []
	for row in array:
		if (isinstance(row, list)):
			flatArray += row
		else:
			flatArray.append(row)
	return flatArray

#Not used
def nanableNumber(array, index):
	if (array[index] == 'NA'):
		array[index] = [0, 1]
	else:
		array[index] = [array[index], 0]
