from distutils.core import setup
from Cython.Build import cythonize

setup(
	name = 'Perceptron',
	ext_modules = cythonize("Perceptron.pyx"),
)