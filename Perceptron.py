import copy

class Perceptron:
	def __init__(self, numInputs):
		self.weights = [1] * (numInputs + 1)
	
	def eval(self, inputs):
		sum = 0
		for i in range(len(self.weights)):
			sum += inputs[i] * self.weights[i]
		
		return Perceptron.sgn(sum)
	
	#Based on Frank Rosenblatt's method
	def learn(self, inputs, output, step):
		evaluatedOutput = self.eval(inputs)
		
		self.weights[0] = self.weights[0] + step * (output - evaluatedOutput) * 1
		
		for i in range(1, len(self.weights)):
			self.weights[i] = self.weights[i - 1] + step * (output - evaluatedOutput) * inputs[i - 1]
	
	def train(self, inputsArray, outputs, step, numIterations):
		#Holds best perceptron found
		#pocket = copy.deepcopy(self)
		
		if len(inputsArray) != len(outputs):
			raise Exception("The inputs and outputs array are not the same length")
		for _ in range(numIterations):
			for i in range(len(inputsArray)):
				self.learn(inputsArray[i], outputs[i], step)
			'''if Perceptron.compare(inputsArray, outputs, pocket, self) == 0:
				pocket = copy.deepcopy(self)'''
		
		#return pocket
	
	def compare(inputsArray, outputs, perceptronA, perceptronB):
		scoreA = 0
		scoreB = 0
		for i in range(len(inputsArray)):
			if perceptronA.eval(inputsArray[i]) == outputs[i]:
				scoreA += 1
			if perceptronB.eval(inputsArray[i]) == outputs[i]:
				scoreB += 1
		if scoreA >= scoreB:
			return 1
		else:
			return 0
	
	def sgn(x):
		if x >= 0:
			return 1
		else:
			return -1

'''if __name__ == "__main__":
	from InputParser import *
	inputsArray, outputsArray = parseFromFile("trainingData.txt")
	outputs = []
	for i in range(len(outputsArray)):
		if outputsArray[i][0] == "COMPLETE_REMISSION":
			outputs.append(1)
		else:
			outputs.append(-1)
	perceptron = Perceptron(len(inputsArray))
	perceptron.train(inputsArray, outputs, 0.01, 1000)
	
	score = 0
	for i in range(len(inputsArray)):
		if perceptron.eval(inputsArray[i]) == outputs[i]:
			score += 1
	file = open("perceptron.txt", "w")
	file.write(repr(perceptron.weights))'''