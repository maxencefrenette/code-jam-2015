from InputParser import *
from NeuralNetwork import NeuralNetwork
from math import exp
from time import time
from random import sample

# Playground code to test the learning algorithm
# Tries to learn f(x) = sin(x) as a test for NeuralNetwork.train()
# Uncomment by adding a hashtag in front of the next line to play with it
"""
from math import sin, pi
import matplotlib.pyplot as plt

net = NeuralNetwork.randomWeights(1, [10, 1]);

n = 10 # amount of training points
inputsArray = [[x/n] for x in range(round(n*2*pi))]
outputsArray = [[sin(inputs[0])] for inputs in inputsArray]

def errorFct(expectedOutputs, actualOutputs):
    return (expectedOutputs[0] - actualOutputs[0])**2

def train(n, learningRate, momentum):
    for i in range(n):
        net.train(inputsArray, outputsArray, errorFct, learningRate, momentum)
        if i % 10 == 0:
            print(error())

def error():
    return net.error(inputsArray, outputsArray, errorFct)

# Will only work if matplotlib is installed
def graph():
    plt.plot([i[0] for i in inputsArray], [o[0] for o in outputsArray])
    plt.plot([i[0] for i in inputsArray], [net.eval(i) for i in inputsArray])
    plt.show()
    
#"""

inputsArray, outputsArray = parseFromFile('trainingData.txt')
net = NeuralNetwork.randomWeights(290, [3])
net.layers[-1].nodes[1].f = lambda x: x
net.layers[-1].nodes[2].f = lambda x: x

def sigmoid(x):
    return 1/(1+exp(-x))

# Returns the points lost in terms of the relative error between 2 numbers
def errorCurve(x):
    return 1-2**(-20*x)

def errorFct(expectedOutputs, actualOutputs):
    error = 0
    
    if expectedOutputs[0] == "COMPLETE_REMISSION":
        error += (1/3)*sigmoid(-actualOutputs[0])
        error += (1/3)*errorCurve(abs((expectedOutputs[1]-actualOutputs[1])/expectedOutputs[1]))
    elif expectedOutputs[0] == "RESISTANT":
        error += (2/3)*sigmoid(actualOutputs[0])
    else:
        raise Exception("Bruh !")
    
    error += (1/3)*errorCurve(abs((expectedOutputs[2]-actualOutputs[2])/expectedOutputs[2]))
    
    return error/10

def error():
    return net.error(inputsArray[:10], outputsArray[:10], errorFct)

def train(runningTime, learningRate, momentum):
    start = time()
    iterations = 0
    
    while time() - start < runningTime:
        net.train(sample(inputsArray, 10), sample(outputsArray, 10), errorFct, learningRate, momentum)
        iterations += 1
    
    print(iterations, " training iterations completed.")
    print("New expected score :", 100.0*(1-error()))

#train(1, 0.01, 0.5)