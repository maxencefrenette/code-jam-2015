import InputParser

def arrayCheck(array):
    floats = 0
    otherStrings = {}
    
    for str in array:
        try:
            float(str)
            floats += 1
            
        except:
            if not str in otherStrings:
                otherStrings[str] = 0
            otherStrings[str] += 1
    if floats != 0:
        print("There are", floats, "numbers in the array.")
    for s in otherStrings:
        print("There are", otherStrings[s], "times the string", s, "in the array.")

def getColumn(array, j):
    return [row[j] for row in array]

def isRectangular(array):
    for row in array:
        if len(row) != len(array[0]):
            return False
    
    return True

inputsArray, outputsArray = InputParser.parseFromFile('trainingData.txt')

print("inputsArray is rectangular :", isRectangular(inputsArray))
print("outputsArray is rectangular :", isRectangular(outputsArray))

for i in range(len(inputsArray)):
    print('Column', i)
    print(arrayCheck(getColumn(inputsArray, i)))