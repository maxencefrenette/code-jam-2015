from Layer import Layer

epsilon = 0.00001

class NeuralNetwork:
	def __init__(self, layers):
		self.layers = layers
	
	def randomWeights(numInputs, layerWidths):
		layers = [Layer.randomWeights(numInputs, layerWidths[0])]
		
		for i in range(1, len(layerWidths)):
			# A layer's input length corresponds to the preceding layer's width
			layers.append(  Layer.randomWeights(layerWidths[i-1], layerWidths[i])  )
		
		return NeuralNetwork(layers)
	
	def eval(self, inputs):
		if len(inputs) + 1 != len(self.layers[0].nodes[0].weights):
			raise TypeError("Wrong inputs size")
		
		result = inputs
		
		for l in self.layers:
			result = l.eval(result)
		
		return result
	
	# learningRate is the gradient descent coefficient. It determines how much the weights will be modified
	# momentum determines how much previous changes will affect the next change
	def train(self, inputsArray, outputsArray, errorFct, learningRate, momentum):
		gradient = []
		error1 = self.error(inputsArray, outputsArray, errorFct)
		
		for i in range(len(self.layers)):
			layer = self.layers[i]
			gradient.append([])
			for j in range(len(layer.nodes)):
				node = layer.nodes[j]
				gradient[i].append([])
				for k in range(len(node.weights)):
					node.weights[k] += epsilon
					error2 = self.error(inputsArray, outputsArray, errorFct)
					node.weights[k] -= epsilon
					
					gradient[i][j].append((error2 - error1)/epsilon)
		
		for i in range(len(self.layers)):
			layer = self.layers[i]
			for j in range(len(layer.nodes)):
				node = layer.nodes[j]
				for k in range(len(node.weights)):
					change = -learningRate*gradient[i][j][k] + momentum*node.changes[k]
					node.changes[k] = change
					node.weights[k] += change
		
		return gradient
	
	def error(self, inputsArray, outputsArray, errorFct):
		error = 0
		
		for i in range(len(inputsArray)):
			inputs = inputsArray[i];
			expectedOutputs = outputsArray[i];
			actualOutputs = self.eval(inputs)
			
			error += errorFct(expectedOutputs, actualOutputs)
		
		return error
	
	def __repr__(self):
		return "NeuralNetwork(" + repr(self.layers) + ")"