from Node import Node

class Layer:
	def __init__(self, nodes):
		self.nodes = nodes
	
	def randomWeights(numInputs, numNodes):
		nodes = [Node.randomWeights(numInputs) for _ in range(numNodes)]
		return Layer(nodes)
	
	def eval(self, inputs):
		if len(inputs) + 1 != len(self.nodes[0].weights):
			raise TypeError("Wrong inputs size")
		
		return [n.eval(inputs) for n in self.nodes]
	
	def __repr__(self):
		return "Layer(" + repr(self.nodes) + ")"