
perceptron = Perceptron(len(weights))
perceptron.weights = weights

def dot(W, X):
    return sum([w*x for w, x in zip(W, X)])

while True:
    try:
        line = input()
    except Exception:
        #Stop if there is no more patient
        break
    if (line == ""):
        break
    id, inputs, outputs = parseLine(line, False)
    output = perceptron.eval(inputs)
    
    if(output > 0):
        print(id, "COMPLETE_REMISSION", dot([1]+inputs, weights1), dot([1]+inputs, weights2), "\t")
    else:
        print(id, "RESISTANT", "NA", dot([1]+inputs, weights2), sep="\t")
