from random import random
from math import tanh

class Node:
	def __init__(self, weights):
		self.weights = weights
		self.changes = [0 for w in weights]
		self.f = tanh
	
	def randomWeights(numInputs):
		weights = [0.2*(2*random() - 1) for _ in range(numInputs+1)]
		return Node(weights)
	
	def eval(self, inputs):
		if len(inputs) + 1 != len(self.weights):
			raise TypeError("Wrong inputs size")
		
		sum = self.weights[0]
		
		for i in range(len(inputs)):
			sum += self.weights[i+1] * inputs[i]
		
		return self.f(sum)
	
	def __repr__(self):
		str = "Node(["
		
		for w in self.weights[:-1]:
			str += ("%.6g" % (w,)) + ","
		
		str += ("%.6g" % (self.weights[-1],)) + "])"
		
		return str