from NeuralNetwork import NeuralNetwork
from InputParser import *
from math import *
from time import *
import numpy as np

net1 = NeuralNetwork.randomWeights(290, [1])
net1.layers[0].nodes[0].f = lambda x: x

net2 = NeuralNetwork.randomWeights(290, [1])
net2.layers[0].nodes[0].f = lambda x: x

inputsArray, outputsArray = parseFromFile('trainingData.txt')

inputsArray1 = []
outputsArray1 = []
for i in range(166):
    if outputsArray[i][1] != 0:
        inputsArray1.append(inputsArray[i])
        outputsArray1.append(outputsArray[i][1])

inputsArray2 = inputsArray
outputsArray2 = [outputs[2] for outputs in outputsArray]

for row in inputsArray:
    row.insert(0, 1)

weights1 = np.linalg.lstsq(inputsArray1, outputsArray1)[0]
weights2 = np.linalg.lstsq(inputsArray2, outputsArray2)[0]

file1 = open('w1.txt', 'w')
file1.write(str(weights1.tolist()))
file1.close()

file2 = open('w2.txt', 'w')
file2.write(str(weights2.tolist()))
file2.close()

"""def errorCurve(x):
    return 1-2**(-20*x)

def errorFct(expectedOutputs, actualOutputs):
    return errorCurve(abs((expectedOutputs[0]-actualOutputs[0])/expectedOutputs[0]))

def printError():
    print("Score 2 :", 100*(1 - net1.error(inputsArray1, outputsArray1, errorFct)/115))
    print("Score 3 :", 100*(1 - net2.error(inputsArray2, outputsArray2, errorFct)/166))

def train(runningTime, learningRate, momentum):
    start = time()
    iterations = 0
    
    while time() - start < runningTime:
        net1.train(inputsArray1, outputsArray1, errorFct, learningRate, momentum)
        net2.train(inputsArray2, outputsArray2, errorFct, learningRate, momentum)
        iterations += 1
    
    print(iterations, " training iterations completed.")
    printError()"""